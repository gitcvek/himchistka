<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends DaFrontendController {
	public function init() {
		if (Yii::app()->request->url == '/') {
			$this->layout = 'webroot.themes.business.views.layouts.main';
		} else {
			$this->layout = 'webroot.themes.business.views.layouts.second';
		}
	}
}
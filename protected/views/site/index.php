<div class = "b-about">
	<div class = "b-about-img col-md-6">
		<img src = "/themes/business/gfx/photo.png" alt = "">
	</div>
	<div class = "b-about-text col-md-6">
		<h1>Весь спектр услуг по химической чистке одежды и изделий</h1>

		<p>Мы используем самое современное оборудование,
		   технологии и химматериалы от ведущих европейских производителей</p>

		<p class = "big">Постоянным клиентам скидка 10%</p>
	</div>
</div>
<div class = "b-services col-md-12">
	<div class = "row">
			<div class = "b-services-item col-md-4">
			<img src = "/themes/business/gfx/content_icon1.png" alt = "">

			<p>Чистка одежды из текстильных материалов, меха, натуральной кожи и замши</p>
		</div>
	

		<div class = "b-services-item col-md-4">
			<img src = "/themes/business/gfx/content_icon2.png" alt = "">

			<p>Индивидуальная чистка эксклюзивных моделей одежды, свадебных платьев</p>
		</div>

		<div class = "b-services-item col-md-4">
			<img src = "/themes/business/gfx/content_icon3.png" alt = "">

			<p>Чистка ковров, паласов, напольных покрытий</p>
		</div>

	</div>
	<div class = "row">
		<div class = "b-services-item col-md-4">
			<img src = "/themes/business/gfx/content_icon4.png" alt = "">

			<p>Химическая чистка и стирка спецодежды</p>
		</div>
		<div class = "b-services-item col-md-4">
			<img src = "/themes/business/gfx/content_icon5.png" alt = "">

			<p>Доставка, выездное обслуживание</p>
		</div>
		<div class = "b-services-item col-md-4">
			<img src = "/themes/business/gfx/content_icon6.png" alt = "">

			<p>Поставка, монтаж и все виды ремонта оборудования прачечных</p>
		</div>
	</div>
</div>
<div class = "b-adresses col-md-12">
	<h1 class = "b-adresses-title">Адреса приемных пунктов</h1>

	<div class = "row">
		<div class = "b-adresses-item col-md-4">
			
<script src="http://api-maps.yandex.ru/2.0-stable/?load=package.standard&lang=ru-RU" type="text/javascript"> </script>
<script type="text/javascript">
			 ymaps.ready(function(){
var myMap = new ymaps.Map("map-canvas1", {
        center: [61.67310075, 50.82611250],
        zoom: 16
    });
myMap.controls.add('smallZoomControl');
var myPlacemark = new ymaps.Placemark([61.67310075, 50.82611250]);
myMap.geoObjects.add(myPlacemark);
});
</script>
	<div class="b-map" id="map-canvas1"></div>
			<p>г.Сыктывкар,<br>ул. Первомайская, 41,<br>тел. 24-21-58<br>пон.-пятн.: 8:00 - 19:00<br>сб.: 8:00 -
			   17:00<br>воскр.: выходной</p>
		</div>
		<div class = "b-adresses-item col-md-4">
			<script type="text/javascript">
			 ymaps.ready(function(){
var myMap = new ymaps.Map("map-canvas2", {
        center: [61.66944175, 50.83753950],
        zoom: 16
    });
myMap.controls.add('smallZoomControl');
var myPlacemark = new ymaps.Placemark([61.66944175, 50.83753950]);
myMap.geoObjects.add(myPlacemark);
});
</script>
<div class="b-map" id="map-canvas2"></div>
			<p>г.Сыктывкар,<br>ул. Ленина, 55,<br>тел. 24-23-97<br>пон.-пятн.: 8:00 - 19:00<br>сб.: 8:00 - 17:00<br>воскр.:
			   выходной</p>
		</div>
		<div class = "b-adresses-item col-md-4">
					<script type="text/javascript">
			 ymaps.ready(function(){
var myMap = new ymaps.Map("map-canvas3", {
        center: [61.78518725, 50.75661950],
        zoom: 16
    });
myMap.controls.add('smallZoomControl');
var myPlacemark = new ymaps.Placemark([61.78518725, 50.75661950]);
myMap.geoObjects.add(myPlacemark);
});
</script>
<div class="b-map" id="map-canvas3"></div>

			<p>Эжва<br>ул. Мира, 68/2, ТЦ "Фрегат",<br>тел. 63-95-42<br>пон.-пятн.: 9.00-19.00<br>сб.: 9.00-17.00<br>воск.:
			   выходной</p>
		</div>
	</div>
</div>

<style>#content .page-header {display:none;}</style>